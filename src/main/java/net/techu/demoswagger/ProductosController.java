package net.techu.demoswagger;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.techu.data.Producto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductosController {

    private ArrayList<Producto> listaProductos;

    public ProductosController(){
        listaProductos = new ArrayList<Producto>();
        listaProductos.add(new Producto("ABC", "Producto 1", 123.22, "CAT1"));
        listaProductos.add(new Producto("DEF", "Producto 2", 1855.4, "CAT2"));
        listaProductos.add(new Producto("GHI", "Producto 3", 123.22, "CAT3"));
    }

    @GetMapping("/productos")
    public ResponseEntity<ArrayList<Producto>> getListaProductos(){
        return new ResponseEntity<ArrayList<Producto>>(listaProductos, HttpStatus.OK);
    }

    @PostMapping("/productos")
    @ApiOperation(value="Crear producto", notes="Este método crea un producto")
    public ResponseEntity<Producto> addProducto(@ApiParam(name="producto",
                                                        type="Producto",
                                                        value="producto a crear",
                                                        example="PR1",
                                                        required=true) @RequestBody Producto productoNuevo){
        listaProductos.add(productoNuevo);
        return new ResponseEntity<Producto>(productoNuevo, HttpStatus.CREATED);
    }

    @PostMapping("/productos/byname")
    @ApiOperation(value="Crear producto", notes="Este método crea un producto solo con el nombre")
    public ResponseEntity<Producto> addProductoByName(@ApiParam(name="producto",
            type="Producto",
            value="producto a crear",
            example="PR1",
            required=true) @RequestParam String nombre){
        Producto productoNuevo = new Producto("123", nombre, 125, "CATX");
        listaProductos.add(productoNuevo);
        return new ResponseEntity<Producto>(productoNuevo, HttpStatus.CREATED);
    }

}
